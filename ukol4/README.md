# 3 - NAT Network Configuration
## Assignment
Proveďte nastavení „malé domácí“ sítě připojené do Internetu přes
dva směrovače a NAT podle uvedeného schématu s simulátoru GNS3. Použijte
uvedený adresní prostor, který rozdělíte na dvě podsítě podle doporučení ve
schématu.

<img src="images/assignment.jpg" width="600" />

K realizaci využijte simulátoru GNS3, který jste si nastavili v rámci „nultého“
úkolu. Rozdělení adresního prostoru si můžete usnadnit programem IPCALC.

Jako OS směrovačů můžete použít jak VyOS, tak Cisco IOS. Cílem je, aby z uzlů
PC1 a PC2 byly dostupné veřejné servery v Internetu, tedy aby fungoval např.
ping na www.seznam.cz.

## Installation
1. Install GNS3 with VirtualBox as described here: https://github.com/maxotta/kiv-psi/

2. Download VyOS image from: https://downloads.vyos.io/?dir=rolling/current/amd64, 
    - We will use VyOS because its  open-source unlike Cisco IOS
    - Tested version is **VyOS 1.4-rolling-202105110417** (vyos-rolling-latest.iso from 2021-05-12)
    
3. Install VyOS template to GNS3 as described here: https://docs.vyos.io/en/latest/installation/virtual/gns3.html
    - if occurred error about VT-x check this: https://stackoverflow.com/a/57229749/5240077
    
## Configuration

1. Start VM and GNS3 and create new project

2. Add devices to workplace as on image in assignment and start all devices

    <img src="images/step1.jpg" width="800" />

    #### Addressing

3. Addressing

    Our address space is **192.168.123.0/24**. We need 2 subnets, one for 20 hosts second for 2 hosts.
    For division of address space we can use program IPCALC:
    
    ```
    ipcalc 192.168.123.0/24 -s 20 2
    ```
    The result will be:
    ```
    Address:   192.168.123.0        11000000.10101000.01111011. 00000000
    Netmask:   255.255.255.0 = 24   11111111.11111111.11111111. 00000000
    Wildcard:  0.0.0.255            00000000.00000000.00000000. 11111111
    =>
    Network:   192.168.123.0/24     11000000.10101000.01111011. 00000000
    HostMin:   192.168.123.1        11000000.10101000.01111011. 00000001
    HostMax:   192.168.123.254      11000000.10101000.01111011. 11111110
    Broadcast: 192.168.123.255      11000000.10101000.01111011. 11111111
    Hosts/Net: 254                   Class C, Private Internet
    
    1. Requested size: 20 hosts
    Netmask:   255.255.255.224 = 27 11111111.11111111.11111111.111 00000
    Network:   192.168.123.0/27     11000000.10101000.01111011.000 00000
    HostMin:   192.168.123.1        11000000.10101000.01111011.000 00001
    HostMax:   192.168.123.30       11000000.10101000.01111011.000 11110
    Broadcast: 192.168.123.31       11000000.10101000.01111011.000 11111
    Hosts/Net: 30                    Class C, Private Internet
    
    2. Requested size: 2 hosts
    Netmask:   255.255.255.252 = 30 11111111.11111111.11111111.111111 00
    Network:   192.168.123.32/30    11000000.10101000.01111011.001000 00
    HostMin:   192.168.123.33       11000000.10101000.01111011.001000 01
    HostMax:   192.168.123.34       11000000.10101000.01111011.001000 10
    Broadcast: 192.168.123.35       11000000.10101000.01111011.001000 11
    Hosts/Net: 2                     Class C, Private Internet
    
    Needed size:  36 addresses.
    Used network: 192.168.123.0/26
    Unused:
    192.168.123.36/30
    192.168.123.40/29
    192.168.123.48/28
    192.168.123.64/26
    192.168.123.128/25
    ```
    So we can now assign addresses to our devices like that:
    
    | Device        | Port      | Network            | Address        |
    | ------------- | ----------| ------------------ | -------------- |
    | PC1           | eth0      | 192.168.123.0/27   | 192.168.123.2  |
    | PC2           | eth0      | 192.168.123.0/27   | 192.168.123.3  |
    | R2            | eth0      | 192.168.123.0/27   | 192.168.123.1  |
    | R2            | eth1      | 192.168.123.32/30  | 192.168.123.33 |
    | R1            | eth0      | 192.168.123.32/30  | 192.168.123.34 |
    | R1            | eth1      | DHCP               | DHCP           |
    
    <img src="images/step2.jpg" width="800" />
    
4. Configuration
    
    Now we can config all devices. Right click on device and choose console to open telnet connection.

    - PC1
    ```
    ip 192.168.123.2/27 192.168.123.1
    ip dns 8.8.8.8
    save
    ```
   
   - PC2
   ```
   ip 192.168.123.3/27 192.168.123.1
   ip dns 8.8.8.8
   save
   ```

    - R2
    ```
    configure
    set system host-name R2
  
    set interfaces ethernet eth0 address 192.168.123.1/27
    set interfaces ethernet eth1 address 192.168.123.33/30
  
    set protocols static route 0.0.0.0/0 next-hop 192.168.123.34
  
    commit
    save
    exit
    ```
   
    We can check our configuration of R2:
    ```
    show interfaces 
    ```
    With result of:
    ```
    Interface        IP Address                        S/L  Description
    ---------        ----------                        ---  -----------
    eth0             192.168.123.1/27                  u/u
    eth1             192.168.123.33/30                 u/u
    ```
    And:
    ```
    show ip route 
    ```
    With result of:
    ```
    S>* 0.0.0.0/0 [1/0] via 192.168.123.34, eth1, weight 1, 01:02:43
    C>* 192.168.123.0/27 is directly connected, eth0, 02:40:13
    C>* 192.168.123.32/30 is directly connected, eth1, 01:02:44
    ```
   
    We can now check configuration of network 192.168.123.0/27 by ping between PC1, PC2 and R2.
    
    - R1
    ```
    configure
    set system host-name R1

    set interfaces ethernet eth1 address dhcp
    set interfaces ethernet eth0 address 192.168.123.34/30

    set system name-server 8.8.8.8
    set system name-server 185.43.135.1   

    set protocols static route 192.168.123.0/27 next-hop 192.168.123.33

    set nat source rule 100 outbound-interface 'eth1'
    set nat source rule 100 source address '192.168.123.0/24'
    set nat source rule 100 translation address masquerade
       
    commit
    save
    exit
    ```
    Sometimes after that its needed to refresh dhcp configuration by:
    ```
    renew dhcp interface eth1
    ```
    We can now check routes:
    ```
    show ip route
    ```
    There may be something like this:
    ```
    S>* 0.0.0.0/0 [210/0] via 192.168.122.1, eth1, weight 1, 00:00:25
    C>* 192.168.122.0/24 is directly connected, eth1, 00:00:28
    S>* 192.168.123.0/27 [1/0] via 192.168.123.33, eth0, weight 1, 00:02:15
    C>* 192.168.123.32/30 is directly connected, eth0, 00:02:45
    ```
    We can see default gateway auto configured by dhcp.
    And check interfaces:
    ```
    show interfaces 
    ```
    With result of:
    ```
    Interface        IP Address                        S/L  Description
    ---------        ----------                        ---  -----------
    eth0             192.168.123.34/30                 u/u
    eth1             192.168.122.161/24                u/u
    ```
      
## Test
Now we can check if internet connection is available from PC1 and PC2. Samples from PC1:

- Ping to www.seznam.cz 

    ```
    ping www.seznam.cz
    ```
    ![](images/test1.jpg)
    
    We can capture this with Wireshark (on link from R1 to NAT1):
    
    ![](images/test2.jpg)
    

#### Useful links
https://docs.vyos.io/en/latest/installation/virtual/gns3.html
https://docs.vyos.io/en/latest/installation/install.html

https://docs.vyos.io/en/latest/configuration/interfaces/ethernet.html
https://docs.vyos.io/en/latest/configuration/protocols/static.html
https://docs.vyos.io/en/latest/configuration/system/name-server.html
https://docs.vyos.io/en/latest/configuration/system/default-route.html

https://protechgurus.com/how-to-use-vpcs-in-gns3/
https://www.youtube.com/watch?v=_6bluxgGZG0&ab_channel=MikeWConsulting
