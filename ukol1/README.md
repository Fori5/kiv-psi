
# 1 - Multicast client
## Assignment
Realizujte IPv4 UDP multicast klienta ve vámi zvoleném programovacím jazyce.  
Pokud máte doma SmartTV nebo jiná podobná DLNA zařízení, můžete si  
vyzkoušet protokol SSDP – Simple Service Discovery Protocol.  
Pomocí zaslání vhodného požadavku můžete zjišťovat jaká UPnP zařízení se  
nacházejí ve vaší domácí síti.
## Description
It's simple [SSDP](https://en.wikipedia.org/wiki/Simple_Service_Discovery_Protocol) client that sends SSDP discovery request for discovering devices and services on local network. Each response is printed on the console. The timeout for waiting for all reponses is set to 5 seconds.

## Implementation
Client is implemented in Python using BSD [socket](https://docs.python.org/3/library/socket.html).

## Run
To run client just run:

    python main.py
### Example output
    Reponse from:('192.168.0.1', 1900)
    HTTP/1.1 200 OK
    CACHE-CONTROL: max-age=300
    DATE: Sun, 25 Apr 2021 13:21:10 GMT
    EXT:
    LOCATION: http://192.168.0.1:1900/gatedesc.xml
    OPT: "http://schemas.upnp.org/upnp/1/0/"; ns=01
    01-NLS: 1e6fb000-1dd2-11b2-b026-e3849dbf9bf0
    SERVER: Linux/2.6.36, UPnP/1.0, Portable SDK for UPnP devices/1.6.19
    X-User-Agent: redsonic
    ST: upnp:rootdevice
    USN: uuid:9f0865b3-f5da-4ad5-85b7-7404637fdf37::upnp:rootdevice
