import socket

MCAST_GRP = '239.255.255.250'
MCAST_PORT = 1900

msg = \
    'M-SEARCH * HTTP/1.1\r\n' \
    'HOST:' + MCAST_GRP + ':' + str(MCAST_PORT) + '\r\n' \
    'ST:upnp:rootdevice\r\n' \
    'MX:2\r\n' \
    'MAN:"ssdp:discover"\r\n' \
    '\r\n'

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.settimeout(5)
s.sendto(msg.encode(), (MCAST_GRP, MCAST_PORT))

try:
    while True:
        data, addr = s.recvfrom(65507)
        print('Reponse from:' + str(addr))
        print(data.decode())
except socket.timeout:
    pass
