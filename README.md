# KIV/PSI
## Úkol 1 - Multicast klient
See: [ukol1](ukol1)
## Úkol 2 - Vícevláknový TCP server
See: [ukol2](ukol2)
## Úkol 3 - Rest klient
See: [ukol3](ukol3)
## Úkol 4 - Nastavení NAT sítě
See: [ukol4](ukol4)
