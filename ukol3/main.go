package main

import (
	"log"
	"time"
)

type Result struct {
	Latitude      string
	Longitude     string
	Time          time.Time
	Day           bool
	AfterSunset   bool
	BeforeSunrise bool
}

func calculateSunriseSunset(issNow *IssNow) (*Result, error) {
	tm := time.Unix(issNow.Timestamp, 0).UTC()

	// fetch sunrise/sunset data for today
	sunriseSunsetToday, err := getSunriseSunset(issNow.Position, tm)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	sunriseToday, err := time.Parse(time.RFC3339, sunriseSunsetToday.Sunrise)
	sunsetToday, err := time.Parse(time.RFC3339, sunriseSunsetToday.Sunset)

	// need check yesterdays sunset
	lookAfter := false
	// need check tomorrows sunrise
	lookBefore := false
	sunriseSunsetYesterday := &SunriseSunset{}
	sunriseSunsetTomorrow := &SunriseSunset{}
	sunsetYesterday := time.Time{}
	sunriseTomorrow := time.Time{}

	// if current time is before today sunrise, we need fetch data for yesterday to check if
	// current time is 1-2 hours afters yesterday sunset
	if tm.Before(sunriseToday) {
		lookBefore = true
		sunriseSunsetYesterday, err = getSunriseSunset(issNow.Position, tm.AddDate(0, 0, -1))
		if err != nil {
			log.Println(err)
			return nil, err
		}
		sunsetYesterday, _ = time.Parse(time.RFC3339, sunriseSunsetYesterday.Sunset)
	}

	// if current time is after today sunset, we need fetch data for tomorrow to check if
	// current time is 1-2 hours before tomorrow sunrise
	if tm.After(sunsetToday) {
		lookAfter = true
		sunriseSunsetTomorrow, err = getSunriseSunset(issNow.Position, tm.AddDate(0, 0, 1))
		if err != nil {
			log.Println(err)
			return nil, err
		}

		sunriseTomorrow, _ = time.Parse(time.RFC3339, sunriseSunsetTomorrow.Sunrise)
	}

	day := false
	beforeSunrise := false
	afterSunset := false

	// night before today sunrise
	if lookBefore {
		day = false
		diff := sunriseToday.Sub(tm).Minutes()
		beforeSunrise = diff >= 60 && diff <= 120
		diff = tm.Sub(sunsetYesterday).Minutes()
		afterSunset = diff >= 60 && diff <= 120
		// night after today sunset
	} else if lookAfter {
		day = false
		diff := tm.Sub(sunsetToday).Minutes()
		afterSunset = diff >= 60 && diff <= 120
		diff = sunriseTomorrow.Sub(tm).Minutes()
		beforeSunrise = diff >= 60 && diff <= 120
		// day (before today sunrise and today sunset)
	} else {
		day = true
		beforeSunrise = false
		afterSunset = false
	}

	return &Result{
		Latitude:      issNow.Position.Latitude,
		Longitude:     issNow.Position.Longitude,
		Time:          tm,
		Day:           day,
		AfterSunset:   afterSunset,
		BeforeSunrise: beforeSunrise,
	}, nil
}

func main() {
	log.Println("Fetching data...")

	// fetch iss location and time
	issNow, err := getIssNow()
	if err != nil {
		log.Println(err)
		return
	}

	result, err := calculateSunriseSunset(issNow)
	if err != nil {
		log.Println(err)
		return
	}

	dayStr := "Day"
	if !result.Day {
		dayStr = "Night"
	}

	conditionsStr := "Bad"
	if result.BeforeSunrise {
		conditionsStr = "Good (1-2 hours before sunrise)"
	} else if result.AfterSunset {
		conditionsStr = "Good (1-2 hours after sunset)"
	}

	println("ISS Location:")
	println("Latitude:", issNow.Position.Latitude)
	println("Longitude:", issNow.Position.Longitude)
	println("Time", result.Time.Format("2006-01-02 15:04:05"))
	println("Day/Night:", dayStr)
	println("Observation conditions:", conditionsStr)
}
