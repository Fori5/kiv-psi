# 3 - Rest client
## Assignment
Implementujte REST klienta, který zjistí aktuální pozici mezinárodní vesmírné stanice (ISS) a
zobrazí informaci o tom, zda se nachází na aktuálně osvětlené či neosvětlené straně Země.
Stejně tak vypíše i informaci o tom, zda jsou na daném místě ideální podmínky pro její
pozorování z povrchu Země, tj. 1-2 hodiny před východem nebo po západu Slunce.
## Description
Simple rest client that check location of International Space Station (ISS) using
[International Space Station Current Location Api](http://open-notify.org/Open-Notify-API/ISS-Location-Now/).
Then using [Sunrise Sunset Api](https://sunrise-sunset.org/api) is determined if the ISS is
on the light (day) or dark (night) side of the Earth. Observation conditions are calculated to check 
if its 1-2 hours before sunrise or 1-2 hours after sunset (at ISS position).
## Implementation
The server is implemented in Go using [Resty](https://github.com/go-resty/resty) library.

## Build
First check if golang is installed. Then run:
```
go build ukol3
```
ukol3.exe file should be created.

## Run
Run ukol3.exe created in previous step, or run:
```
go run ukol3
```

## Usage
Just run app and see response, e.g.:
```
ISS Location:
Latitude: -49.0249
Longitude: -17.4104
Time 2021-04-25 19:23:45
Day/Night: Night
Observation conditions: Good (1-2 hours after sunset)
```
