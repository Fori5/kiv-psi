package main

import (
	"encoding/json"
	"errors"
	"github.com/go-resty/resty/v2"
	"time"
)

const IssNowUrl = "http://api.open-notify.org/iss-now.json"
const SunriseSunsetUrl = "https://api.sunrise-sunset.org/json"

type LatLon struct {
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

type IssNow struct {
	Timestamp int64  `json:"timestamp"`
	Message   string `json:"message"`
	Position  LatLon `json:"iss_position"`
}

func getIssNow() (*IssNow, error) {
	client := resty.New()
	response, err := client.R().Get(IssNowUrl)
	if err != nil {
		return nil, err
	}

	body := response.Body()
	if body == nil {
		return nil, errors.New("EMPTY BODY")
	}

	issNow := &IssNow{}
	err = json.Unmarshal(body, &issNow)
	if err != nil {
		return nil, err
	}

	return issNow, nil
}

type SunriseSunsetResponse struct {
	Results SunriseSunset `json:"results"`
	Status  string        `json:"status"`
}

type SunriseSunset struct {
	Sunrise                   string `json:"sunrise"`
	Sunset                    string `json:"sunset"`
	SolarNoon                 string `json:"solar_noon"`
	DayLength                 int    `json:"day_length"`
	CivilTwilightBegin        string `json:"civil_twilight_begin"`
	CivilTwilightEnd          string `json:"civil_twilight_end"`
	NauticalTwilightBegin     string `json:"nautical_twilight_begin"`
	NauticalTwilightEnd       string `json:"nautical_twilight_end"`
	AstronomicalTwilightBegin string `json:"astronomical_twilight_begin"`
	AstronomicalTwilightEnd   string `json:"astronomical_twilight_end"`
}

func getSunriseSunset(position LatLon, time time.Time) (*SunriseSunset, error) {
	client := resty.New()

	response, err := client.R().
		SetQueryParams(map[string]string{
			"lat":       position.Latitude,
			"lng":       position.Longitude,
			"date":      time.Format("2006-01-02 15:04:05"), // https://golang.org/pkg/time/#Time.Format
			"formatted": "0",
		}).
		Get(SunriseSunsetUrl)
	if err != nil {
		return nil, err
	}

	body := response.Body()
	if body == nil {
		return nil, errors.New("EMPTY BODY")
	}

	sunriseSunsetResponse := &SunriseSunsetResponse{}
	err = json.Unmarshal(body, &sunriseSunsetResponse)
	if err != nil {
		return nil, err
	}

	if sunriseSunsetResponse.Status != "OK" {
		return nil, errors.New(sunriseSunsetResponse.Status)
	}

	return &sunriseSunsetResponse.Results, nil
}
