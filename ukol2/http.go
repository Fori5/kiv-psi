package main

import (
	"bufio"
	"errors"
	"io"
	"strconv"
	"strings"
)

type HttpRequest struct {
	method  string
	uri     string
	version string
}

type HttpResponseCode struct {
	code int
	desc string
}

type HttpResponse struct {
	version string
	code    HttpResponseCode
	content string
}

func parseRequest(rd io.Reader) (*HttpRequest, error) {
	r := bufio.NewReader(rd)
	line, _, err := r.ReadLine()

	if err != nil {
		return nil, err
	}

	parts := strings.Split(string(line), " ")
	if len(parts) != 3 {
		return nil, errors.New("INVALID REQUEST")
	}

	request := &HttpRequest{
		method:  parts[0],
		uri:     parts[1],
		version: parts[2],
	}

	return request, nil
}

func responseToBytes(response *HttpResponse) []byte {
	bytes := []byte(response.version + " " + strconv.Itoa(response.code.code) + " " + response.code.desc)
	bytes = append(bytes, "\r\n\r\n"...)
	bytes = append(bytes, response.content...)
	return bytes
}
