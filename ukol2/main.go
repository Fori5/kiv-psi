package main

import (
	"os"
	"strconv"
)

const DefaultPort = 8080

func main() {
	args := os.Args[1:]
	port := DefaultPort
	if len(args) > 0 {
		port, _ = strconv.Atoi(args[0])
	}

	runServer(port)
}
