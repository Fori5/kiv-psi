package main

import (
	"log"
	"net"
	"os"
	"strconv"
)

func closeSocket(socket net.Listener) {
	err := socket.Close()
	if err != nil {
		log.Println(err)
	}
}

func runServer(port int) {
	socket, err := net.Listen("tcp", "localhost"+":"+strconv.Itoa(port))
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	defer closeSocket(socket)
	log.Println("Server listening on port " + strconv.Itoa(port))

	for {
		conn, err := socket.Accept()
		log.Println("Connection: ", conn.RemoteAddr())
		if err != nil {
			log.Println(err)
			continue
		}

		go handleRequest(conn)
	}
}

func handleRequest(conn net.Conn) {
	request, err := parseRequest(conn)
	if err != nil {
		log.Println("Error reading:", err)
		return
	}

	log.Println("request:", request)
	response := getResponse(request)
	log.Println("response:", response)

	_, _ = conn.Write(responseToBytes(response))

	err = conn.Close()
	if err != nil {
		log.Println("Error closing client socket", err)
	}
}

func getResponse(request *HttpRequest) *HttpResponse {
	if request.version != "HTTP/1.1" || request.method != "GET" {
		return &HttpResponse{
			version: "HTTP/1.1",
			code: HttpResponseCode{
				code: 400,
				desc: "Bad request",
			},
			content: "",
		}
	}

	return getRouteResponse(request.uri)
}
