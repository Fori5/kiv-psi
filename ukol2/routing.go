package main

func rootRoute() *HttpResponse {
	return &HttpResponse{
		version: "HTTP/1.1",
		code: HttpResponseCode{
			code: 200,
			desc: "OK",
		},
		content: "<html><body><h1>Welcome to HTTP server,</h1> more about this can be found <a href=\"/about\">here</a>.</body></html>",
	}
}

func aboutRoute() *HttpResponse {
	return &HttpResponse{
		version: "HTTP/1.1",
		code: HttpResponseCode{
			code: 200,
			desc: "OK",
		},
		content: "<html><body><h1>About</h1>check <a target=\"_blank\" href=\"https://gitlab.com/Fori5/kiv-psi/-/tree/master/ukol2\">repository</a></body></html>",
	}
}

func getRouteResponse(route string) *HttpResponse {
	switch route {
	case "/":
		return rootRoute()
	case "/about":
		return aboutRoute()
	default:
		return &HttpResponse{
			version: "HTTP/1.1",
			code: HttpResponseCode{
				code: 404,
				desc: "Not found",
			},
			content: "<html><body>404 - Not found</body></html>",
		}
	}
}
