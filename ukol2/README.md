
# 1 - Multithreaded HTTP server
## Assignment
Implementujte vícevláknový TCP server, který je schopen generovat jednoduché odpovědi
na požadavky HTTP klienta (pouze metoda GET).
Programová struktura serveru odpovídá struktuře „klasického“ serveru využívajícího
rozhraní BSD socketů.
## Description
It's simple HTTP server that runs every client in own thread (goroutine). 

It supports only HTTP/1.1 version and GET method. 
Response is simple html content for supported routes:
- /
- /about
- every other gets "404 Not found" response

## Implementation
The server is implemented in Go using TCP sockets.

## Build
First check if golang is installed. Then run:
```
go build ukol2
```
ukol2.exe file should be created.

## Run
Run ukol2.exe created in previous step, or run:
```
go run ukol2
```
You can pass port to listen on, default one is 8080, e.g.:
```
go run ukol2 1234
```

## Usage
After running app, just open http://localhost:8080 in web browser.

